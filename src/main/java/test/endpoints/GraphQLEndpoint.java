package test.endpoints;

import com.coxautodev.graphql.tools.SchemaParser;
import javax.servlet.annotation.WebServlet;

import graphql.schema.GraphQLSchema;
import graphql.servlet.SimpleGraphQLServlet;
import test.repositories.BookRepository;
import test.repositories.LinkRepository;
import test.resolvers.*;

import java.sql.*;

@WebServlet(urlPatterns = "/graphql")
public class GraphQLEndpoint extends SimpleGraphQLServlet{
    private static final LinkRepository linkRepository;
    private static final BookRepository bookRepository;
    static final String CONNECTION_URL = "jdbc:mysql://localhost:3306/graphql_test?user=root&serverTimezone=UTC";
    private static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
    private static Connection connection;


    static {
        try {
            connection = DriverManager.getConnection(CONNECTION_URL);
        } catch (SQLException e) {
            e.printStackTrace();
        }

        linkRepository = new LinkRepository();
        bookRepository = new BookRepository(connection);
    }


    public GraphQLEndpoint(){
        super(buildSchema());
    }

    private static GraphQLSchema buildSchema(){
        return SchemaParser.newParser()
                .file("schema.graphqls")
                .resolvers(new Query(linkRepository, bookRepository), new Mutation(linkRepository, bookRepository))
                .build()
                .makeExecutableSchema();
    }
}

