package test.repositories;

import test.dataModels.Link;

import java.util.ArrayList;
import java.util.List;

public class LinkRepository {
    private final List<Link> links;

    public LinkRepository() {
        links = new ArrayList<>();

        links.add(new Link("http://google.com/", "descdescdescdescdesc"));
        links.add(new Link("http://stackoverflow.com", "csedcsedcsedcsed"));
    }

    public List<Link> getAllLinks(){
        return links;
    }

    public void saveLink(Link link){
        links.add(link);
    }
}
