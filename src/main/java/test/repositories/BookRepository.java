package test.repositories;

import test.dataModels.Book;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class BookRepository {
    private ResultSet DbBooks;
    private List<Book> books;
    private final Connection connection;

    public BookRepository(Connection connection) {
        this.connection = connection;
        books = new ArrayList<>();

        getBooksFromDb();
    }

    public List<Book> getAllBooks(){
        getBooksFromDb();

        return books;
    }

    public Book findById(String id){
        for(Book b : books){
            if(b.getId().compareTo(id) == 0)
                return b;
        }

        return null;
    }

    public void saveBook(Book book){
        try {
            Statement statement = connection.createStatement();
            statement.execute("INSERT INTO Book VALUES('" + book.getId() + "', '" + book.getTitle() + "', '" + book.getAuthor() + "')");
        } catch (SQLException e) {
            e.printStackTrace();
        }

        getBooksFromDb();
    }

    private void saveToBookList(ResultSet DbBooks){
        books.clear();

        try {
            while (DbBooks.next()) {
                books.add(new Book(DbBooks.getString("id"),
                                DbBooks.getString("title"),
                                DbBooks.getString("author")));
            }
        }
        catch (SQLException ex){
            ex.getStackTrace();
        }
    }

    private void getBooksFromDb(){
        try {
            Statement statement = connection.createStatement();
            DbBooks = statement.executeQuery("SELECT * FROM Book");
        } catch (SQLException e) {
            e.printStackTrace();
        }

        saveToBookList(DbBooks);
    }
}
