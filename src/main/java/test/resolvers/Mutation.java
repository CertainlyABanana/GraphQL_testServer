package test.resolvers;

import com.coxautodev.graphql.tools.GraphQLMutationResolver;
import test.dataModels.*;
import test.repositories.*;

public class Mutation implements GraphQLMutationResolver {
    private final LinkRepository linkRepository;
    private final BookRepository bookRepository;

    public Mutation(LinkRepository linkRepository, BookRepository bookRepository){
        this.linkRepository = linkRepository;
        this.bookRepository = bookRepository;
    }

    public Link createLink(String url, String desc){
        Link newLink = new Link(url,desc);
        linkRepository.saveLink(newLink);
        return newLink;
    }

    public Book createBook(String id, String title, String author){
        Book newBook = new Book(id, title, author);
        bookRepository.saveBook(newBook);
        return newBook;
    }


}
