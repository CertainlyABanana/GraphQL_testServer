package test.resolvers;

import com.coxautodev.graphql.tools.GraphQLQueryResolver;
import test.dataModels.*;
import test.repositories.*;

import java.util.List;

public class Query implements GraphQLQueryResolver{
    private final LinkRepository linkRepository;
    private final BookRepository bookRepository;

    public Query(LinkRepository linkRepository, BookRepository bookRepository){
        this.linkRepository = linkRepository;
        this.bookRepository = bookRepository;
    }

    public List<Link> allLinks(){
        return linkRepository.getAllLinks();
    }

    public List<Book> allBooks(){
        return bookRepository.getAllBooks();
    }

    public Book findBookById(String id){
        return bookRepository.findById(id);
    }
}
