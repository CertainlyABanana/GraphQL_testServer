# General
IDE: IntelliJ IDEA 2017.3.5     
Run it with `mvn jetty:run`    
Test queries at `http://localhost:8080/`

# Query details
**Methods:**     
`name(param: type!): returnType`    
`!` - required field    

**Fields:**    
`name: type`

**Available queries:**
> allLinks: Link    
> allBooks: Book    
> findBookById(id: String!): Book

**Available mutations:**
> createLink(url: String!, description: String!): Link    
> createBook(id: String!, title: String!, author: String!): Book

**Available data classes and fields:**

- Link    
-- url: String!     
-- description: String!     

   
- Book     
-- id: String!    
-- title: String!    
-- author: String!    

# Database config
Best hosted on XAMPP(7.2.3) server, which uses `MariaDB 10.1.31` which is based on `MySQL 5.6/5.7`

Database name: `graphql_test`     
Database user: `root`     
No password

